//--------------------- require
const mysql = require("mysql");

//--------------------- conection to databases
var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "dbcovidgt",
});

//--------------------- Export connection
module.exports = connection;
