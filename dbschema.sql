CREATE DATABASE dbcovidgt;
USE dbcovidgt;
CREATE TABLE `casos` (
  `dep` varchar(30),
  `positivos` int,
  `recuperados` int,
  `fallecidos` int
)