//--------------------- require
const express = require("express");
const bodyParser = require("body-parser");

var app = express();
const connection = require("./dbconect");
app.use(bodyParser.json());

//--------------------- Cors method
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

//--------------------- Mian
app.get("/", (request, response) => {
  response.send("Bienvendio al API, ingresa a /covidgt");
});

//--------------------- Send data and consult data
app.post("/covidgt", (request, response) => {
  const dep = request.body;
  var nombre = dep.nombre;
  connection.query(
    "SELECT * FROM casos WHERE dep = ?",
    [nombre],
    (err, result) => {
      if (err) throw err;
      response.json(result);
    }
  );
});

//--------------------- Export routes
module.exports = app;
