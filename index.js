//--------------------- require
const express = require("express");

//--------------------- Star varibale
const app = express();
const appRoutes = require("./routes");

//--------------------- Reques to server
app.use(express.urlencoded({ extended: false }));
app.use("/", appRoutes);

//--------------------- Port to run server
port = process.env.PORT || 3000;
console.log("Puerto", port);
app.listen(port);
